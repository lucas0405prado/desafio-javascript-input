const nameInput = document.querySelector("#name")
const mailInput = document.querySelector("#mail")
const buttonSubmit = document.querySelector("#submit")
const textValidation = document.querySelector("#validationText")
const mailValidation = document.querySelector("#validationMail")
const textLogin = document.querySelector("#loginAceito")
const sectionLogin = document.querySelector("main")
const loginText = document.querySelector("#login")

let nameUser = ""
let mailUser = ""
let mailInvalid = false
let nameInvalid = false
let textError = ""





buttonSubmit.addEventListener("click", logar);

function logar() {

    coletarDados ()
    validarNome ()
    validarMail()

    verificarLogin()
  


}



let coletarDados = () => {

    nameUser = nameInput.value
    mailUser = mailInput.value
}

let validarNome = () => {

    const caracteres = /[0-9]/;

    if ( caracteres.test(nameUser)) {
        nameInvalid = true
        textError = "Insira apenas letras em seu nome"
    } else if (nameUser == "") {
        nameInvalid = true
        textError = "Insira um valor em nome"

    } else  {
        nameInvalid = false
        textError = ""
        
    }
        textValidation.innerHTML = textError 
}

let validarMail = () => {

    const emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/gi

    if ( mailUser == "") {
        mailInvalid = true
        textError = "Insira um valor em email"
    } else if ( emailRegex.test(mailUser) == false ) {
        mailInvalid = true
        textError = "Insira um email válido"
        console.log("anus")
    } else {
        mailInvalid = false
        textError = ""
        
    }


        mailValidation.innerHTML = textError
   

}

let verificarLogin = () => {

    if (nameInvalid == false && mailInvalid == false) {
        executarTransicao()
    }

}

let executarTransicao = () => {

    nameInput.disabled = true
    mailInput.disabled = true
    textLogin.innerHTML = "Login Autenticado..."
    textLogin.style.animationName = "transitionLogin"
    setTimeout (  ( ) => {

        sectionLogin.style.animationName = "menuTransition"

        setTimeout (  (  ) => {
            loginText.style.animationName = "transitionLogin"
            loginText.innerHTML = `Seja Bem-vindo ${nameUser} com email ${mailUser}`
        } , 500   )

    }, 1500  )
}





